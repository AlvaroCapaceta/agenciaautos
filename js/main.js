const interes = {
  12: 1.125,
  18: 1.172,
  24: 1.21,
  36: 1.26,
  48: 1.45,
};

const calcular = () => {
  const valor = document.getElementById("txtValor").value;
  const meses = document.getElementById("cmbPlanes").value;

  if (valor <= 0) {
    alert("Ingrese un valor del automovil correcto");
    return;
  }

  let enganche = valor * 0.3;
  let financiar = (valor - enganche) * interes[meses];
  let pago = financiar / meses;

  document.getElementById("txtEnganche").value = "$ " + enganche.toFixed(3);
  document.getElementById("txtFinanciar").value = "$ " + financiar.toFixed(3);
  document.getElementById("txtPago").value = "$ " + pago.toFixed(3);
};
